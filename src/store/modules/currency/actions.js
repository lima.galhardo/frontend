import axios from "axios";
export default {
  async listCurrencies({ commit }, currency) {
    const resp = await axios.get(
      `https://swop.cx/rest/rates/${currency}/BRL?api-key=56132eaead48a4add8692447428d48359254d9c182f489817543586e02fba73e`
    );
    commit("SET_CURRENCY", resp.data);
  },
};
