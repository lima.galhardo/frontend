import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

import currency from "./modules/currency";

export default new Vuex.Store({
  modules: {
    currency,
  },
});
